import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { ContactosComponent } from './contactos/contactos.component';
import { Prueba1Component } from './prueba1/prueba1.component';
import { TestComponent } from './test/test.component';



@NgModule({
  declarations: [
    InicioComponent,
    ServiciosComponent,
    GaleriaComponent,
    ContactosComponent,
    Prueba1Component,
    TestComponent
  ],
  imports: [
    CommonModule
  ]
})
export class RoutesModule { }
